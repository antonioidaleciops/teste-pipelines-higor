import React from 'react';

import { Container } from './styles';

const SuccessCard = ({ children }) => <Container>{children}</Container>;

export default SuccessCard;
