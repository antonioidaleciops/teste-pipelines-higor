import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Routes from '../../routes';
import { Navbar } from '../../components';

import { Container } from './styles';

const AppLayout = () => (
  <Container>
    <BrowserRouter basename="/teste-pipelines-higor">
      <div className="navbar">
        <Navbar />
      </div>
      <main className="content">
        <Routes />
      </main>
    </BrowserRouter>
  </Container>
);

export default AppLayout;
